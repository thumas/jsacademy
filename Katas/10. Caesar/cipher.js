const alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
const alphabet2 = 'abcdefghijklmnopqrstuvwxyz'

const charSwapper = (char, key) => {
    let res;
    let swapped = false
    for (let i = 0; i < alphabet2.length; i++) {
        if (char === alphabet2[i]) {
            res = alphabet2[(i + key) % 26]
            swapped = true;
        }
    }
    if (!swapped) {
        for (let i = 0; i < alphabet.length; i++) {
            if (char === alphabet[i]) {
                res = alphabet[(i + key) % 26]
                swapped = true;
            }
        }
        if (!swapped) {
            if (char === " ") {
                res = " "
            }
            else if (char === "-") {
                res = "-"
            }
        }

    }
    return res
}

const cipher = (string, num) => {
    let ciphered = ""
    for (i = 0; i < string.length; i++) {
        ciphered += charSwapper(string[i], num)
    }
    return ciphered
}

console.log(cipher("Always-Look-on-the-Bright-Side-of-Life", 5))
