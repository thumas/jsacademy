function fizzBuzz(x){
    if ((x % 3 === 0) && (x % 5 === 0)){
        console.log("FizzBuzz")
    }
    else if (x % 3 === 0){
        console.log("Fizz")
    }
    else if ( x % 5 === 0){
        console.log("Buzz")
    }
    else {
        console.log(x)
    }
}

let x = Number(window.prompt('Enter a number'))
for (let i = 1; i < x; i++){
    fizzBuzz(i)
}