function deNest (list){

    if (list.length === 0){
        return 0;
    } else if (Array.isArray(list[0])){
        return deNest(list[0]) + deNest(list.slice(1,list.length));      
    } else {
        return 1 + deNest(list.slice(1,list.length));
    };

};
console.log(deNest([1, [2, 3]])) 
console.log(deNest(([1, [2, [3, 4]]])))
console.log(deNest([1, [2, [3, [4, [5, 6]]]]]))
console.log(deNest([1, [2], 1, [2], 1]))