function splitOnDoubleLetter(inWord){
    word = inWord.toLowerCase()

    const wordRay = []
    let lastSplit = 0

    for (let i = 0; i < word.length; i++){
        if (word[i] === word[i-1]) {
            wordRay.push(word.slice(lastSplit, i))
            
            lastSplit = i  
        }
    }
    if (lastSplit != 0){
        wordRay.push(word.slice(lastSplit, word.length)) 
    }
    return wordRay
}

console.log(splitOnDoubleLetter('tesst'))
console.log(splitOnDoubleLetter('mississippi'))
console.log(splitOnDoubleLetter('easy'))
console.log(splitOnDoubleLetter('llllllllllllllllllll'))