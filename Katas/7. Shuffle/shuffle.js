function outShuffle (cards){
    let bottomHalf = cards.slice(0, cards.length/2)
    let topHalf = cards.slice(bottomHalf.length, cards.length)
    let shuffledCards = []

    for (let i = 0; i < bottomHalf.length; i++){
        shuffledCards.push(bottomHalf[i])
        shuffledCards.push(topHalf[i])
    }
    console.log(shuffledCards)
    return shuffledCards
}

function shuffleCount(size){
    let cards = []
    for (let i = 1; i < size+1 ; i++){
        cards.push(i)
    }
    originalCards = cards;
    counter = 0;
    do{ cards = outShuffle(cards)
        counter++;


    } while(!arrayEquals(cards, originalCards))
    return counter
}

function arrayEquals(first, second){
    let equals = true
    for (let i = 0; i < first.length; i++){
        if ( first[i] != second[i] ){
            equals = false
        }
    }
    return equals

}

console.log(shuffleCount(10))