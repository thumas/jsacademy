const one = 59314215
const two = 0
const three = 4214
const four = 29
const five = 12490

const digisCount = (numbers) => {
    if (numbers < 10){
        return 1
    }
    return 1+digisCount(numbers/10)
}
console.log(`${one}: ${digisCount(one)}`)
console.log(`${two}: ${digisCount(two)}`)
console.log(`${three}: ${digisCount(three)}`)
console.log(`${four}: ${digisCount(four)}`)
console.log(`${five}: ${digisCount(five)}`)