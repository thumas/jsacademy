const case1 = ([

    ["X", "O", "X"],
  
    ["O", "X",  "O"],
  
    ["O", "X",  "X"]
  
  ])
const case2 = ([
  
    ["O", "O", "O"],
  
    ["O", "X", "X"],
  
    ["E", "X", "X"]
  
  ])
const case3= ([

    ["X", "X", "O"],
  
    ["O", "O", "X"],
  
    ["X", "X", "O"]
  
  ])
const case4 = ([

    ["X", "O", "O"],
  
    ["O", "X", "X"],
  
    ["O", "X", "X"]
  
  ])


function ticTacToe (arr){
    for (let i = 0; i < 3 ; i++){
        // Rows
        if ((arr[i][0] === arr[i][1]) && (arr[i][0] === arr[i][2])){
            return arr[i][0]
        // Columns
        }else if ((arr[0][i] === arr[1][i]) && (arr[0][i] === arr[2][i])){
            return arr[0][i]
        }
    // backslash
    if ((arr[0][0] === arr[1][1]) && (arr[0][0] == arr[2][2])){
        return arr[1][1]
    //forwardslash
    }else if ((arr[0][2] === arr[1][1]) && (arr[0][2] === arr[2][0])){
        return arr[1][1]
    // Draw
    }else{
        return 'draw'
        }

    }
}

console.log(ticTacToe(case1))
console.log(ticTacToe(case2))
console.log(ticTacToe(case3))
console.log(ticTacToe(case4))