function boomerang (arr){
    let counter = 0;
    for (let i = 0; i < arr.length-2 ; i++){
        if ( (Number(arr[i]) === Number(arr[i+2])) && (Number(arr[i]) != Number(arr[i+1]))){
            counter ++;
        }
    }
    return counter
}
console.log(boomerang([3, 7, 3, 2, 1, 5, 1, 2, 2, -2, 2])) //3
console.log(boomerang([9, 5, 9, 5, 1, 1, 1])) //2
console.log(boomerang([5, 6, 6, 7, 6, 3, 9])) // 1
console.log(boomerang([4, 4, 4, 9, 9, 9, 9])) //➞ 0
console.log(boomerang([1, 7, 1, 7, 1, 7, 1])) //➞ 5
