
// Returns a string, all lowercase and removes everything that is not alphabet letters
const stringCollapser = (string) => {
    let result = ""
    for (let i = 0; i < string.length; i++) {
        if (isLetter(string[i])) {
            result += string[i]
        }
    }
    return result.toLowerCase()

}

// checks if a character is a letter and returns true or false
const isLetter = (letter) => {
    return (letter.toLowerCase() != letter.toUpperCase())
}

// compares two strings and checks if they're an anagram
const isAnagram = (string1, string2) => {
    if (string1.length != string2.length) {
        return false
    }
    // removes characters from the ref string if they are found in string1, returns true if the ref string is empty at the end
    let ref = string2
    for (i = 0; i < string1.length; i++) {
        let counted = false;
        for (j = 0; j < ref.length; j++) {
            if ((string1[i] === ref[j]) && (counted === false)) {
                ref = ref.replace(ref[j], '')
                counted = true

            }
        }
    }
    if (ref.length === 0) {
        return true
    }
    return false

}
// finds an anagam within a string
const hiddenAnagram = (string, anagram) => {
    collapsed = stringCollapser(string)
    for (let i = 0; i < collapsed.length; i++) {
        let anagramRef = stringCollapser(anagram)
        let snippet = collapsed.slice(i, i + anagramRef.length)
        if (isAnagram(anagramRef, snippet)) {
            return snippet
        }
    }
    return "noutfound"
}


console.log(hiddenAnagram("An old west action hero actor", "Clint Eastwood"))
console.log(hiddenAnagram("Mr. Mojo Rising could be a song title", "Jim Morrison"))
console.log(hiddenAnagram("Banana? margaritas", "ANAGRAM"))
console.log(hiddenAnagram("D e b90it->?$ (c)a r...d,,#~", "bad credit"))
console.log(hiddenAnagram("Bright is the moon", "Bongo mirth"))
