# Komputer Store

## Name
Komputer Store

## Maintainer
GitLab- @thumas, 

## Description
Assignment given by Noroff.
* User can choose to buy one of the listed computers with money earned from "working" with the work button and banked with the bank button.
* By banking the money, the user can then take a loan or attempt to buy a computer.
* A loan can maximum be double the current balance in the bank.
* Only 1 loan can be taken.
* If the user already has a loan, up to 10% of the banked salary will be paid as interest.
* If the user wants to pay off their loan, they can either do it by paying 10% passivly or all at once with the "pay back loan" button.

## Installation
Preferable if Live Server (extension) has been installed in Visual Studio Code.

## Usage
If Live Server(extension) is installed: Right-click on index.html at the explorer and choose "Open with Live Server".
A new browser window will then open, user can now try out the Komputer Store by selecting a computer and buy it after having earned some money with the "Work" button.

## Project status
Finished project after following the NorOff assignment criteria.
