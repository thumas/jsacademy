const bankButton = document.getElementById("bank-button")
const workButton = document.getElementById("work-button")
const loanButton = document.getElementById("loan-button")
const repayButton = document.getElementById("repay-button")
const buyButton = document.getElementById("buy-button")
const computersElement = document.getElementById("komputer")
const priceElement = document.getElementById("price")
const descriptionElement = document.getElementById("description")
const specsElement = document.getElementById("specs")
const stockElement = document.getElementById("stock")
const imgElement = document.getElementById("kompimg")

let payment = 0;
let balance = 0;
let loanedStatus = 0;
let debt = 0;
let selectedPrice = 0;
let computers = []

// API fetch, to get data for the different Komputers
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToDropDown(computers))

// Calls a function for every element to create arrays of data, see below
const addComputersToDropDown = (computers) => {
    computers.forEach(x => addSingleComputerToDropDown(x))
}
 // Creates an array of data for the computers   
const addSingleComputerToDropDown = (computer) => {
    const computerElement = document.createElement("option")
    computerElement.value = computer.id
    computerElement.appendChild(document.createTextNode(computer.title))
    computersElement.appendChild(computerElement)
}
// event-listener functions below, connects clicks with the relevant function    

// Bank-button
function bankButtonClick(){
    bankSalary()
}
// Work-button
function workButtonClick(){
    addSalary()
}
// Get a Loan-button
function loanButtonClick(){
        applyForLoan()
}
// Pay back Loan -button
function repayButtonClick(){
    payBack()
}
// Buy Button
function buyButtonClick(){
    buy()

}

// Adds salary when clicking the workbutton
function addSalary(){
    payment += 100
    tick()
}


// Banks your salary and pays interest if you have a loan
function bankSalary(){
    let interest = payment * 0.1
    if (loanedStatus != 0 && interest <= debt){
        payment = payment - interest
        debt = debt - interest
        console.log(`Paid ${interest}kr as interest. `)
    // edge-case to prevent negative debt if 10% of salary is greater than the debt
    } else if (loanedStatus != 0 && interest > debt ){
        payment = payment - debt
        console.log(`Your debt of ${debt}kr has been paid off.`)
        debt = 0
        loanedStatus = 0;
    }
    balance += payment
    console.log(`Banked ${payment}kr.`)
    payment = 0
    tick()
}
//Applies for a loan and takes it if requirements are met: Enough balance and current loaned status
function applyForLoan(){
    let loanReq = Number(window.prompt('Amount'))
    if ((loanReq <= balance*2) && (loanedStatus === 0)){
        // edge case of negative loan requests
        if (loanReq < 0){
            loanReq = 0
            window.alert('Nice try, we are not stupid.')
            return
        }
        balance += loanReq
        console.log(`A loan of ${loanReq}kr was approved.`)
        debt = loanReq
        loanedStatus = 1
    } else if (loanedStatus != 0){
        window.alert('You already have a loan with us, pay your debt and then we can talk again')
    }
    else{
        window.alert('We only approve loans at a maximum double your current balane')
    }
    tick()
}
//Pays back your debt if sufficient balance
function payBack(){
    if (balance > debt){
        balance = balance - debt
        console.log(`Your debt of ${debt}kr was paid off.`)
        debt = 0
        loanedStatus = 0
    }
    tick() 

}
// Selects the computer from the drop-down and updating HTML elements with Price, Description, specs, stock and image
const komputerSelected = e => {
    const selectedComputer = computers[e.target.selectedIndex]
    priceElement.innerHTML = `${selectedComputer.price} kr.`
    descriptionElement.innerHTML = selectedComputer.description
    specsElement.innerHTML = specsLister(selectedComputer.specs)
    stockElement.innerHTML = `${selectedComputer.stock} in stock.`
    imgElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedComputer.image
    selectedPrice = selectedComputer.price
}
// re-formats specs into a readable list
function specsLister (specs){
    let specString = ""
    for (i = 0; i < specs.length ; i++ ){
        specString += `<br> * ${specs[i]} <br>`
    }
    return specString

}
// attempts to buy the selected komputer and alerts the user if the purchase was successful or not
function buy(){
    if ( balance >= selectedPrice){
        balance -= selectedPrice
        window.alert('congrats on your purchase, you now own a new computer!')
    }
    else {
        window.alert('You cannot affor that right now, work some more or get a loan first!')
    }
tick()
}

// Updates all data when called upon. Is called with every interaction in the program
function tick(){
    document.getElementById("payment").innerHTML = payment
    document.getElementById("balance").innerHTML = balance

    document.getElementById("joe-banker").innerHTML = "Joe Banker"
    if (loanedStatus > 0){
        document.getElementById("joe-banker").innerHTML = "JOE LOANSHARK"
        document.getElementById("repay-button").style.display = "block"
        document.getElementById("debt").innerHTML = `Debt: ${debt}`
        document.getElementById("debt").style.display = "block"
    }
    if (loanedStatus === 0){
        document.getElementById("joe-banker").innerHTML = "Joe Banker"
        document.getElementById("repay-button").style.display = "None"
        document.getElementById("debt").style.display = "None"
    }
}

// startup tick and added eventlisteners
tick()
bankButton.addEventListener('click', bankButtonClick)
workButton.addEventListener('click', workButtonClick)
loanButton.addEventListener('click', loanButtonClick)
repayButton.addEventListener('click', repayButtonClick)
buyButton.addEventListener('click', buyButtonClick)
computersElement.addEventListener('change', komputerSelected)


